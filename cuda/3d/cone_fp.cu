/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
           2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/
#include <fstream>

#include <cstdio>
#include <cassert>
#include <iostream>
#include <list>

#include <cuda.h>
#include "util3d.h"
#include "helpers.h"

#ifdef STANDALONE
#include "testutil.h"
#endif

#include "dims3d.h"
#include "astra/MPIProjector3D.h"

typedef texture<float, 3, cudaReadModeElementType> texture3D;

//#define N_MAX_MATERIALS 10

static texture3D gT_coneVolumeTexture; // soon to be obsolete. Will be substituted by the following line
//cudaTextureObject_t gT_coneVolumeTextureList[N_MAX_MATERIALS];

namespace astraCUDA3d {

static const unsigned int g_anglesPerBlock = 4;

// thickness of the slices we're splitting the volume up into
static const unsigned int g_blockSlices = 32;
static const unsigned int g_detBlockU = 32;
static const unsigned int g_detBlockV = 32;

static const unsigned int g_volBlockX = 32;
static const unsigned int g_volBlockY = 2;
static const unsigned int g_volBlockZ = 32;

static const unsigned g_MaxAngles = 1024;
__constant__ float gC_SrcX[g_MaxAngles];
__constant__ float gC_SrcY[g_MaxAngles];
__constant__ float gC_SrcZ[g_MaxAngles];
__constant__ float gC_DetSX[g_MaxAngles];
__constant__ float gC_DetSY[g_MaxAngles];
__constant__ float gC_DetSZ[g_MaxAngles];
__constant__ float gC_DetUX[g_MaxAngles];
__constant__ float gC_DetUY[g_MaxAngles];
__constant__ float gC_DetUZ[g_MaxAngles];
__constant__ float gC_DetVX[g_MaxAngles];
__constant__ float gC_DetVY[g_MaxAngles];
__constant__ float gC_DetVZ[g_MaxAngles];


bool bindVolumeDataTexture(const cudaArray* array)
{
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();

	gT_coneVolumeTexture.addressMode[0] = cudaAddressModeBorder;
	gT_coneVolumeTexture.addressMode[1] = cudaAddressModeBorder;
	gT_coneVolumeTexture.addressMode[2] = cudaAddressModeBorder;
	gT_coneVolumeTexture.filterMode = cudaFilterModeLinear;
	gT_coneVolumeTexture.normalized = false; 

	cudaBindTextureToArray(gT_coneVolumeTexture, array, channelDesc);

	// TODO: error value?

	return true;
}

bool bindArrayDataTexture(cudaTextureObject_t& d_coneVolumeTexture, const cudaArray_t d_volumeArray)
{
	d_coneVolumeTexture = {0};

	// create texture object
	cudaResourceDesc texRes;
	memset(&texRes, 0, sizeof(cudaResourceDesc));
    texRes.resType            = cudaResourceTypeArray;
    texRes.res.array.array    = d_volumeArray;

	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(cudaTextureDesc));
	texDesc.addressMode[0] = cudaAddressModeBorder;
	texDesc.addressMode[1] = cudaAddressModeBorder;
	texDesc.addressMode[2] = cudaAddressModeBorder;
	texDesc.filterMode = cudaFilterModeLinear;
	//texDesc.filterMode = cudaFilterModePoint;
	texDesc.normalizedCoords = false;
	texDesc.readMode = cudaReadModeElementType;

	ASTRA_CUDA_ASSERT(cudaCreateTextureObject( &d_coneVolumeTexture, &texRes,  &texDesc, NULL));

	return true;
}

// x=0, y=1, z=2
struct DIR_X {
	__device__ float nSlices(const SDimensions3D& dims) const { return dims.iVolX; }
	__device__ float nDim1(const SDimensions3D& dims) const { return dims.iVolY; }
	__device__ float nDim2(const SDimensions3D& dims) const { return dims.iVolZ; }
	__device__ float c0(float x, float y, float z) const { return x; }
	__device__ float c1(float x, float y, float z) const { return y; }
	__device__ float c2(float x, float y, float z) const { return z; }
	__device__ float tex(float f0, float f1, float f2) const { return tex3D(gT_coneVolumeTexture, f0, f1, f2); }
	__device__ float tex(cudaTextureObject_t coneVolumeTexture, float f0, float f1, float f2) const { return tex3D<float>(coneVolumeTexture, f0, f1, f2); }
	__device__ float x(float f0, float f1, float f2) const { return f0; }
	__device__ float y(float f0, float f1, float f2) const { return f1; }
	__device__ float z(float f0, float f1, float f2) const { return f2; }
};

// y=0, x=1, z=2
struct DIR_Y {
	__device__ float nSlices(const SDimensions3D& dims) const { return dims.iVolY; }
	__device__ float nDim1(const SDimensions3D& dims) const { return dims.iVolX; }
	__device__ float nDim2(const SDimensions3D& dims) const { return dims.iVolZ; }
	__device__ float c0(float x, float y, float z) const { return y; }
	__device__ float c1(float x, float y, float z) const { return x; }
	__device__ float c2(float x, float y, float z) const { return z; }
	__device__ float tex(float f0, float f1, float f2) const { return tex3D(gT_coneVolumeTexture, f1, f0, f2); }
	__device__ float tex(cudaTextureObject_t coneVolumeTexture, float f0, float f1, float f2) const { return tex3D<float>(coneVolumeTexture, f1, f0, f2); }
	__device__ float x(float f0, float f1, float f2) const { return f1; }
	__device__ float y(float f0, float f1, float f2) const { return f0; }
	__device__ float z(float f0, float f1, float f2) const { return f2; }
};

// z=0, x=1, y=2
struct DIR_Z {
	__device__ float nSlices(const SDimensions3D& dims) const { return dims.iVolZ; }
	__device__ float nDim1(const SDimensions3D& dims) const { return dims.iVolX; }
	__device__ float nDim2(const SDimensions3D& dims) const { return dims.iVolY; }
	__device__ float c0(float x, float y, float z) const { return z; }
	__device__ float c1(float x, float y, float z) const { return x; }
	__device__ float c2(float x, float y, float z) const { return y; }
	__device__ float tex(float f0, float f1, float f2) const { return tex3D(gT_coneVolumeTexture, f1, f2, f0); }
	__device__ float tex(cudaTextureObject_t coneVolumeTexture, float f0, float f1, float f2) const { return tex3D<float>(coneVolumeTexture, f1, f2, f0); }
	__device__ float x(float f0, float f1, float f2) const { return f1; }
	__device__ float y(float f0, float f1, float f2) const { return f2; }
	__device__ float z(float f0, float f1, float f2) const { return f0; }
};

	// threadIdx: x = ??? detector  (u?)
	//            y = relative angle

	// blockIdx:  x = ??? detector  (u+v?)
    //            y = angle block


template<class COORD>
__global__ void polyCone_FP_t(float* D_projData, unsigned int projPitch,
                          unsigned int startSlice,
                          unsigned int startAngle, unsigned int endAngle,
                          const SDimensions3D dims, cudaTextureObject_t coneVolumeTexture, float fOutputScale)
{
	COORD c;

	int angle = startAngle + blockIdx.y * g_anglesPerBlock + threadIdx.y;
	if (angle >= endAngle)
		return;

	const float fSrcX = gC_SrcX[angle];
	const float fSrcY = gC_SrcY[angle];
	const float fSrcZ = gC_SrcZ[angle];
	const float fDetUX = gC_DetUX[angle];
	const float fDetUY = gC_DetUY[angle];
	const float fDetUZ = gC_DetUZ[angle];
	const float fDetVX = gC_DetVX[angle];
	const float fDetVY = gC_DetVY[angle];
	const float fDetVZ = gC_DetVZ[angle];
	const float fDetSX = gC_DetSX[angle] + 0.5f * fDetUX + 0.5f * fDetVX;
	const float fDetSY = gC_DetSY[angle] + 0.5f * fDetUY + 0.5f * fDetVY;
	const float fDetSZ = gC_DetSZ[angle] + 0.5f * fDetUZ + 0.5f * fDetVZ;

	const int detectorU = (blockIdx.x%((dims.iProjU+g_detBlockU-1)/g_detBlockU)) * g_detBlockU + threadIdx.x;
	const int startDetectorV = (blockIdx.x/((dims.iProjU+g_detBlockU-1)/g_detBlockU)) * g_detBlockV;
	int endDetectorV = startDetectorV + g_detBlockV;
	if (endDetectorV > dims.iProjV)
		endDetectorV = dims.iProjV;

	int endSlice = startSlice + g_blockSlices;
	if (endSlice > c.nSlices(dims))
		endSlice = c.nSlices(dims);

	for (int detectorV = startDetectorV; detectorV < endDetectorV; ++detectorV)
	{
		/* Trace ray from Src to (detectorU,detectorV) from */
		/* X = startSlice to X = endSlice                   */

		const float fDetX = fDetSX + detectorU*fDetUX + detectorV*fDetVX;
		const float fDetY = fDetSY + detectorU*fDetUY + detectorV*fDetVY;
		const float fDetZ = fDetSZ + detectorU*fDetUZ + detectorV*fDetVZ;

		/*        (x)   ( 1)       ( 0) */
		/* ray:   (y) = (ay) * x + (by) */
		/*        (z)   (az)       (bz) */

		const float a1 = (c.c1(fSrcX,fSrcY,fSrcZ) - c.c1(fDetX,fDetY,fDetZ)) / (c.c0(fSrcX,fSrcY,fSrcZ) - c.c0(fDetX,fDetY,fDetZ));
		const float a2 = (c.c2(fSrcX,fSrcY,fSrcZ) - c.c2(fDetX,fDetY,fDetZ)) / (c.c0(fSrcX,fSrcY,fSrcZ) - c.c0(fDetX,fDetY,fDetZ));
		const float b1 = c.c1(fSrcX,fSrcY,fSrcZ) - a1 * c.c0(fSrcX,fSrcY,fSrcZ);
		const float b2 = c.c2(fSrcX,fSrcY,fSrcZ) - a2 * c.c0(fSrcX,fSrcY,fSrcZ);

		const float fDistCorr = sqrt(a1*a1+a2*a2+1.0f) * fOutputScale;

		float fVal = 0.0f;

		float f0 = startSlice + 0.5f;
		float f1 = a1 * (startSlice - 0.5f*c.nSlices(dims) + 0.5f) + b1 + 0.5f*c.nDim1(dims) - 0.5f + 0.5f;
		float f2 = a2 * (startSlice - 0.5f*c.nSlices(dims) + 0.5f) + b2 + 0.5f*c.nDim2(dims) - 0.5f + 0.5f;

		for (int s = startSlice; s < endSlice; ++s)
		{
			fVal += c.tex(coneVolumeTexture, f0, f1, f2);

			//fVal += tex3D<float>(coneVolumeTexture, f0, f1, f2);
			//fVal +=0.001;
			f0 += 1.0f;
			f1 += a1;
			f2 += a2;
		}

		fVal *= fDistCorr;

		D_projData[(detectorV*dims.iProjAngles+angle)*projPitch+detectorU] += fVal;
	}
}


template<class COORD>
__global__ void cone_FP_t(float* D_projData, unsigned int projPitch,
                          unsigned int startSlice,
                          unsigned int startAngle, unsigned int endAngle,
                          const SDimensions3D dims, float fOutputScale)
{
	COORD c;

	int angle = startAngle + blockIdx.y * g_anglesPerBlock + threadIdx.y;
	if (angle >= endAngle)
		return;

	const float fSrcX = gC_SrcX[angle];
	const float fSrcY = gC_SrcY[angle];
	const float fSrcZ = gC_SrcZ[angle];
	const float fDetUX = gC_DetUX[angle];
	const float fDetUY = gC_DetUY[angle];
	const float fDetUZ = gC_DetUZ[angle];
	const float fDetVX = gC_DetVX[angle];
	const float fDetVY = gC_DetVY[angle];
	const float fDetVZ = gC_DetVZ[angle];
	const float fDetSX = gC_DetSX[angle] + 0.5f * fDetUX + 0.5f * fDetVX;
	const float fDetSY = gC_DetSY[angle] + 0.5f * fDetUY + 0.5f * fDetVY;
	const float fDetSZ = gC_DetSZ[angle] + 0.5f * fDetUZ + 0.5f * fDetVZ;

	const int detectorU = (blockIdx.x%((dims.iProjU+g_detBlockU-1)/g_detBlockU)) * g_detBlockU + threadIdx.x;
	const int startDetectorV = (blockIdx.x/((dims.iProjU+g_detBlockU-1)/g_detBlockU)) * g_detBlockV;
	int endDetectorV = startDetectorV + g_detBlockV;
	if (endDetectorV > dims.iProjV)
		endDetectorV = dims.iProjV;

	int endSlice = startSlice + g_blockSlices;
	if (endSlice > c.nSlices(dims))
		endSlice = c.nSlices(dims);

	for (int detectorV = startDetectorV; detectorV < endDetectorV; ++detectorV)
	{
		/* Trace ray from Src to (detectorU,detectorV) from */
		/* X = startSlice to X = endSlice                   */

		const float fDetX = fDetSX + detectorU*fDetUX + detectorV*fDetVX;
		const float fDetY = fDetSY + detectorU*fDetUY + detectorV*fDetVY;
		const float fDetZ = fDetSZ + detectorU*fDetUZ + detectorV*fDetVZ;

		/*        (x)   ( 1)       ( 0) */
		/* ray:   (y) = (ay) * x + (by) */
		/*        (z)   (az)       (bz) */

		const float a1 = (c.c1(fSrcX,fSrcY,fSrcZ) - c.c1(fDetX,fDetY,fDetZ)) / (c.c0(fSrcX,fSrcY,fSrcZ) - c.c0(fDetX,fDetY,fDetZ));
		const float a2 = (c.c2(fSrcX,fSrcY,fSrcZ) - c.c2(fDetX,fDetY,fDetZ)) / (c.c0(fSrcX,fSrcY,fSrcZ) - c.c0(fDetX,fDetY,fDetZ));
		const float b1 = c.c1(fSrcX,fSrcY,fSrcZ) - a1 * c.c0(fSrcX,fSrcY,fSrcZ);
		const float b2 = c.c2(fSrcX,fSrcY,fSrcZ) - a2 * c.c0(fSrcX,fSrcY,fSrcZ);

		const float fDistCorr = sqrt(a1*a1+a2*a2+1.0f) * fOutputScale;

		float fVal = 0.0f;

		float f0 = startSlice + 0.5f;
		float f1 = a1 * (startSlice - 0.5f*c.nSlices(dims) + 0.5f) + b1 + 0.5f*c.nDim1(dims) - 0.5f + 0.5f;
		float f2 = a2 * (startSlice - 0.5f*c.nSlices(dims) + 0.5f) + b2 + 0.5f*c.nDim2(dims) - 0.5f + 0.5f;

		for (int s = startSlice; s < endSlice; ++s)
		{
			fVal += c.tex(f0, f1, f2);
			f0 += 1.0f;
			f1 += a1;
			f2 += a2;
		}

		fVal *= fDistCorr;

		D_projData[(detectorV*dims.iProjAngles+angle)*projPitch+detectorU] += fVal;
	}
}

template<class COORD>
__global__ void cone_FP_SS_t(float* D_projData, unsigned int projPitch,
                             unsigned int startSlice,
                             unsigned int startAngle, unsigned int endAngle,
                             const SDimensions3D dims, float fOutputScale)
{
	COORD c;

	int angle = startAngle + blockIdx.y * g_anglesPerBlock + threadIdx.y;
	if (angle >= endAngle)
		return;

	const float fSrcX = gC_SrcX[angle];
	const float fSrcY = gC_SrcY[angle];
	const float fSrcZ = gC_SrcZ[angle];
	const float fDetUX = gC_DetUX[angle];
	const float fDetUY = gC_DetUY[angle];
	const float fDetUZ = gC_DetUZ[angle];
	const float fDetVX = gC_DetVX[angle];
	const float fDetVY = gC_DetVY[angle];
	const float fDetVZ = gC_DetVZ[angle];
	const float fDetSX = gC_DetSX[angle] + 0.5f * fDetUX + 0.5f * fDetVX;
	const float fDetSY = gC_DetSY[angle] + 0.5f * fDetUY + 0.5f * fDetVY;
	const float fDetSZ = gC_DetSZ[angle] + 0.5f * fDetUZ + 0.5f * fDetVZ;

	const int detectorU = (blockIdx.x%((dims.iProjU+g_detBlockU-1)/g_detBlockU)) * g_detBlockU + threadIdx.x;
	const int startDetectorV = (blockIdx.x/((dims.iProjU+g_detBlockU-1)/g_detBlockU)) * g_detBlockV;
	int endDetectorV = startDetectorV + g_detBlockV;
	if (endDetectorV > dims.iProjV)
		endDetectorV = dims.iProjV;

	int endSlice = startSlice + g_blockSlices;
	if (endSlice > c.nSlices(dims))
		endSlice = c.nSlices(dims);

	const float fSubStep = 1.0f/dims.iRaysPerDetDim;

	for (int detectorV = startDetectorV; detectorV < endDetectorV; ++detectorV)
	{
		/* Trace ray from Src to (detectorU,detectorV) from */
		/* X = startSlice to X = endSlice                   */

		float fV = 0.0f;

		float fdU = detectorU - 0.5f + 0.5f*fSubStep;
		for (int iSubU = 0; iSubU < dims.iRaysPerDetDim; ++iSubU, fdU+=fSubStep) {
		float fdV = detectorV - 0.5f + 0.5f*fSubStep;
		for (int iSubV = 0; iSubV < dims.iRaysPerDetDim; ++iSubV, fdV+=fSubStep) {

		const float fDetX = fDetSX + fdU*fDetUX + fdV*fDetVX;
		const float fDetY = fDetSY + fdU*fDetUY + fdV*fDetVY;
		const float fDetZ = fDetSZ + fdU*fDetUZ + fdV*fDetVZ;

		/*        (x)   ( 1)       ( 0) */
		/* ray:   (y) = (ay) * x + (by) */
		/*        (z)   (az)       (bz) */

		const float a1 = (c.c1(fSrcX,fSrcY,fSrcZ) - c.c1(fDetX,fDetY,fDetZ)) / (c.c0(fSrcX,fSrcY,fSrcZ) - c.c0(fDetX,fDetY,fDetZ));
		const float a2 = (c.c2(fSrcX,fSrcY,fSrcZ) - c.c2(fDetX,fDetY,fDetZ)) / (c.c0(fSrcX,fSrcY,fSrcZ) - c.c0(fDetX,fDetY,fDetZ));
		const float b1 = c.c1(fSrcX,fSrcY,fSrcZ) - a1 * c.c0(fSrcX,fSrcY,fSrcZ);
		const float b2 = c.c2(fSrcX,fSrcY,fSrcZ) - a2 * c.c0(fSrcX,fSrcY,fSrcZ);

		const float fDistCorr = sqrt(a1*a1+a2*a2+1.0f) * fOutputScale;

		float fVal = 0.0f;

		float f0 = startSlice + 0.5f;
		float f1 = a1 * (startSlice - 0.5f*c.nSlices(dims) + 0.5f) + b1 + 0.5f*c.nDim1(dims) - 0.5f + 0.5f;
		float f2 = a2 * (startSlice - 0.5f*c.nSlices(dims) + 0.5f) + b2 + 0.5f*c.nDim2(dims) - 0.5f + 0.5f;

		for (int s = startSlice; s < endSlice; ++s)
		{
			fVal += c.tex(f0, f1, f2);
			f0 += 1.0f;
			f1 += a1;
			f2 += a2;
		}

		fVal *= fDistCorr;
		fV += fVal;

		}
		}

		D_projData[(detectorV*dims.iProjAngles+angle)*projPitch+detectorU] += fV / (dims.iRaysPerDetDim * dims.iRaysPerDetDim);
	}
}


bool PolyConeFP_Array_internal(cudaPitchedPtr D_projData,
	const SDimensions3D& dims, unsigned int angleCount, const SConeProjection* angles, cudaTextureObject_t coneVolumeTexture,
	float fOutputScale)
{
// transfer angles to constant memory
float* tmp = new float[angleCount];

#define TRANSFER_TO_CONSTANT(name) do { for (unsigned int i = 0; i < angleCount; ++i) tmp[i] = angles[i].f##name ; cudaMemcpyToSymbol(gC_##name, tmp, angleCount*sizeof(float), 0, cudaMemcpyHostToDevice); } while (0)

TRANSFER_TO_CONSTANT(SrcX);
TRANSFER_TO_CONSTANT(SrcY);
TRANSFER_TO_CONSTANT(SrcZ);
TRANSFER_TO_CONSTANT(DetSX);
TRANSFER_TO_CONSTANT(DetSY);
TRANSFER_TO_CONSTANT(DetSZ);
TRANSFER_TO_CONSTANT(DetUX);
TRANSFER_TO_CONSTANT(DetUY);
TRANSFER_TO_CONSTANT(DetUZ);
TRANSFER_TO_CONSTANT(DetVX);
TRANSFER_TO_CONSTANT(DetVY);
TRANSFER_TO_CONSTANT(DetVZ);

#undef TRANSFER_TO_CONSTANT

delete[] tmp;

//ASTRA_WARN(" ##############  %f   %f   %f",angles->fDetSX, angles->fDetSY, angles->fDetSZ);
//ASTRA_WARN(" #########arrow  %f   %f   %f",angles->fDetVX, angles->fDetVY, angles->fDetVZ);

std::list<cudaStream_t> streams;
dim3 dimBlock(g_detBlockU, g_anglesPerBlock); // region size, angles

// Run over all angles, grouping them into groups of the same
// orientation (roughly horizontal vs. roughly vertical).
// Start a stream of grids for each such group.

unsigned int blockStart = 0;
unsigned int blockEnd = 0;
int blockDirection = 0;

// timeval t;
// tic(t);

for (unsigned int a = 0; a <= angleCount; ++a) {
int dir = -1;
if (a != angleCount) {
float dX = fabsf(angles[a].fSrcX - (angles[a].fDetSX + dims.iProjU*angles[a].fDetUX*0.5f + dims.iProjV*angles[a].fDetVX*0.5f));
float dY = fabsf(angles[a].fSrcY - (angles[a].fDetSY + dims.iProjU*angles[a].fDetUY*0.5f + dims.iProjV*angles[a].fDetVY*0.5f));
float dZ = fabsf(angles[a].fSrcZ - (angles[a].fDetSZ + dims.iProjU*angles[a].fDetUZ*0.5f + dims.iProjV*angles[a].fDetVZ*0.5f));

if (dX >= dY && dX >= dZ)
  dir = 0;
else if (dY >= dX && dY >= dZ)
  dir = 1;
else
  dir = 2;
}

if (a == angleCount || dir != blockDirection) {
// block done

blockEnd = a;
if (blockStart != blockEnd) {

  dim3 dimGrid(
			   ((dims.iProjU+g_detBlockU-1)/g_detBlockU)*((dims.iProjV+g_detBlockV-1)/g_detBlockV),
(blockEnd-blockStart+g_anglesPerBlock-1)/g_anglesPerBlock);
  // TODO: check if we can't immediately
  //       destroy the stream after use
  cudaStream_t stream;
  cudaStreamCreate(&stream);
  streams.push_back(stream);

  // printf("angle block: %d to %d, %d (%dx%d, %dx%d)\n", blockStart, blockEnd, blockDirection, dimGrid.x, dimGrid.y, dimBlock.x, dimBlock.y);

  if (blockDirection == 0) {
	  for (unsigned int i = 0; i < dims.iVolX; i += g_blockSlices)
		  if (dims.iRaysPerDetDim == 1)
			  polyCone_FP_t<DIR_X><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, coneVolumeTexture,  fOutputScale);
		  else
			  cone_FP_SS_t<DIR_X><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims,  fOutputScale);
  } else if (blockDirection == 1) {
	  for (unsigned int i = 0; i < dims.iVolY; i += g_blockSlices)
		  if (dims.iRaysPerDetDim == 1)
		  	polyCone_FP_t<DIR_Y><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, coneVolumeTexture,  fOutputScale);
		  else
			  cone_FP_SS_t<DIR_Y><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims,  fOutputScale);
  } else if (blockDirection == 2) {
	  for (unsigned int i = 0; i < dims.iVolZ; i += g_blockSlices)
		  if (dims.iRaysPerDetDim == 1)
		  	polyCone_FP_t<DIR_Z><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, coneVolumeTexture, fOutputScale);
		  else
			  cone_FP_SS_t<DIR_Z><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims,  fOutputScale);
  }

}

blockDirection = dir;
blockStart = a;
}
}

for (std::list<cudaStream_t>::iterator iter = streams.begin(); iter != streams.end(); ++iter)
cudaStreamDestroy(*iter);

streams.clear();

cudaTextForceKernelsCompletion();

// printf("%f\n", toc(t));

return true;
}

bool ConeFP_Array_internal(cudaPitchedPtr D_projData,
                  const SDimensions3D& dims, unsigned int angleCount, const SConeProjection* angles,
                  float fOutputScale)
{
	// transfer angles to constant memory
	float* tmp = new float[angleCount];

#define TRANSFER_TO_CONSTANT(name) do { for (unsigned int i = 0; i < angleCount; ++i) tmp[i] = angles[i].f##name ; cudaMemcpyToSymbol(gC_##name, tmp, angleCount*sizeof(float), 0, cudaMemcpyHostToDevice); } while (0)

	TRANSFER_TO_CONSTANT(SrcX);
	TRANSFER_TO_CONSTANT(SrcY);
	TRANSFER_TO_CONSTANT(SrcZ);
	TRANSFER_TO_CONSTANT(DetSX);
	TRANSFER_TO_CONSTANT(DetSY);
	TRANSFER_TO_CONSTANT(DetSZ);
	TRANSFER_TO_CONSTANT(DetUX);
	TRANSFER_TO_CONSTANT(DetUY);
	TRANSFER_TO_CONSTANT(DetUZ);
	TRANSFER_TO_CONSTANT(DetVX);
	TRANSFER_TO_CONSTANT(DetVY);
	TRANSFER_TO_CONSTANT(DetVZ);

#undef TRANSFER_TO_CONSTANT

	delete[] tmp;

	std::list<cudaStream_t> streams;
	dim3 dimBlock(g_detBlockU, g_anglesPerBlock); // region size, angles

	// Run over all angles, grouping them into groups of the same
	// orientation (roughly horizontal vs. roughly vertical).
	// Start a stream of grids for each such group.

	unsigned int blockStart = 0;
	unsigned int blockEnd = 0;
	int blockDirection = 0;

	// timeval t;
	// tic(t);

	for (unsigned int a = 0; a <= angleCount; ++a) {
		int dir = -1;
		if (a != angleCount) {
			float dX = fabsf(angles[a].fSrcX - (angles[a].fDetSX + dims.iProjU*angles[a].fDetUX*0.5f + dims.iProjV*angles[a].fDetVX*0.5f));
			float dY = fabsf(angles[a].fSrcY - (angles[a].fDetSY + dims.iProjU*angles[a].fDetUY*0.5f + dims.iProjV*angles[a].fDetVY*0.5f));
			float dZ = fabsf(angles[a].fSrcZ - (angles[a].fDetSZ + dims.iProjU*angles[a].fDetUZ*0.5f + dims.iProjV*angles[a].fDetVZ*0.5f));

			if (dX >= dY && dX >= dZ)
				dir = 0;
			else if (dY >= dX && dY >= dZ)
				dir = 1;
			else
				dir = 2;
		}

		if (a == angleCount || dir != blockDirection) {
			// block done

			blockEnd = a;
			if (blockStart != blockEnd) {

				dim3 dimGrid(
				             ((dims.iProjU+g_detBlockU-1)/g_detBlockU)*((dims.iProjV+g_detBlockV-1)/g_detBlockV),
(blockEnd-blockStart+g_anglesPerBlock-1)/g_anglesPerBlock);
				// TODO: check if we can't immediately
				//       destroy the stream after use
				cudaStream_t stream;
				cudaStreamCreate(&stream);
				streams.push_back(stream);

				// printf("angle block: %d to %d, %d (%dx%d, %dx%d)\n", blockStart, blockEnd, blockDirection, dimGrid.x, dimGrid.y, dimBlock.x, dimBlock.y);

				if (blockDirection == 0) {
					for (unsigned int i = 0; i < dims.iVolX; i += g_blockSlices)
						if (dims.iRaysPerDetDim == 1)
							cone_FP_t<DIR_X><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, fOutputScale);
						else
							cone_FP_SS_t<DIR_X><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, fOutputScale);
				} else if (blockDirection == 1) {
					for (unsigned int i = 0; i < dims.iVolY; i += g_blockSlices)
						if (dims.iRaysPerDetDim == 1)
							cone_FP_t<DIR_Y><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, fOutputScale);
						else
							cone_FP_SS_t<DIR_Y><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, fOutputScale);
				} else if (blockDirection == 2) {
					for (unsigned int i = 0; i < dims.iVolZ; i += g_blockSlices)
						if (dims.iRaysPerDetDim == 1)
							cone_FP_t<DIR_Z><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, fOutputScale);
						else
							cone_FP_SS_t<DIR_Z><<<dimGrid, dimBlock, 0, stream>>>((float*)D_projData.ptr, D_projData.pitch/sizeof(float), i, blockStart, blockEnd, dims, fOutputScale);
				}

			}

			blockDirection = dir;
			blockStart = a;
		}
	}

	for (std::list<cudaStream_t>::iterator iter = streams.begin(); iter != streams.end(); ++iter)
		cudaStreamDestroy(*iter);

	streams.clear();

	cudaTextForceKernelsCompletion();

	// printf("%f\n", toc(t));

	return true;
}




bool ConeFP(cudaPitchedPtr D_volumeData,
            cudaPitchedPtr D_projData,
            const SDimensions3D& dims2, const SConeProjection* angles,
            float fOutputScale,
            const astra::CMPIProjector3D *mpiPrj = NULL)
{
	//ASTRA_WARN("ConeFP() ::: and then, in dreaming,");
	int 	       zoffset = 0;
	SDimensions3D  dims    = dims2;
	int	  	incr = 0; //todo proper name

#if USE_MPI
	if(mpiPrj)
	{
		//ASTRA_WARN("ConeFP() ::: The clouds methought would open");
		//Modify the volume properties to ignore the ghostcells
		//Modify the height, and change the startpoint of the copy (zoffset)
		int2 ghosts = mpiPrj->getGhostCells();
		dims.iVolZ -= (ghosts.x + ghosts.y);
		zoffset     = ghosts.x;
		//ASTRA_WARN("ConeFP() ::: and show riches,");
		//Now for the projection data ghostcells
		ghosts 	     = mpiPrj->getGhostCellsPrj();
		dims.iProjV -= (ghosts.x + ghosts.y);  
		incr      = ghosts.x * D_projData.pitch * D_projData.ysize;
	}
#endif

#if 0 //debugcode
	SDimensions3D  dims3    = dims;
	//dims3.iVolZ *= nMaterials;
	float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
	copyVolumeFromDevice(pTmpData, D_volumeData, dims3, 0);
	cudaTextForceKernelsCompletion();
	char buff[100];
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	free(pTmpData);
#endif

	// transfer volume to array
	cudaArray* cuArray = allocateVolumeArray(dims);
	transferVolumeToArray(D_volumeData, cuArray, dims, zoffset); //NOTE the zoffset
	bindVolumeDataTexture(cuArray);
	//ASTRA_WARN("ConeFP() ::: ready to drop upon me");
	bool ret;

	for (unsigned int iAngle = 0; iAngle < dims.iProjAngles; iAngle += g_MaxAngles) {
		unsigned int iEndAngle = iAngle + g_MaxAngles;
		if (iEndAngle >= dims.iProjAngles)
			iEndAngle = dims.iProjAngles;

		cudaPitchedPtr D_subprojData = D_projData;
		D_subprojData.ptr = (char*)D_projData.ptr + iAngle * D_projData.pitch + incr;

		ret = ConeFP_Array_internal(D_subprojData,
		                            dims, iEndAngle - iAngle, angles + iAngle,
		                            fOutputScale);
		if (!ret)
			break;
	}

	//ASTRA_WARN("ConeFP() ::: that when I waked");

	cudaFreeArray(cuArray);

#if 0 //debugcode
	SDimensions3D  dims4    = dims;
	float *pTmpData2= (float*)malloc(dims4.iProjU*dims4.iProjAngles*dims4.iProjV*sizeof(float));
	cudaPitchedPtr D_projData3 = D_projData;
	D_projData3.ptr = (char*)D_projData.ptr + incr;
	//ASTRA_WARN("ConeFP() ::: incr: %d",number);
	copyProjectionsFromDevice(pTmpData2, D_projData3, dims4);
	cudaTextForceKernelsCompletion();
	char buff2[100];
	for (int k=0; k<dims4.iProjV; ++k) {
		sprintf (buff2, "/home/snip/tmp/%d/proj/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff2, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData2[k*dims4.iProjU*dims4.iProjAngles], dims4.iProjU*dims4.iProjAngles*sizeof(float));
		myFile.close();
	}
	free(pTmpData2);
#endif

#if USE_MPI
   	  if(mpiPrj)
	  {
		//ASTRA_WARN("ConeFP() ::: I cried to dream again");
	    //Note not changed ptr as the exchange function figures this out itself
	    const_cast<astra::CMPIProjector3D*>(mpiPrj)->exchangeOverlapAndGhostRegions(NULL, D_projData, false, 3);
	    const_cast<astra::CMPIProjector3D*>(mpiPrj)->exchangeOverlapAndGhostRegions(NULL, D_projData, false, 1);
	  }
#endif

//ASTRA_WARN("ConeFP() ::: .");

	return ret;
}


// AWARE ! 
// The way this forward projector is implemented doesn't allow to superimpose the FP of an object to *already existing* projection data
__global__
void calculatePolychromaticAttenuation(
	cudaPitchedPtr projData, int iProjU, int iProjAngles, int iProjV, 
	void* energyResponse, void* matAttenuations, int nEnergyBins, int nMaterials, float sign )
{ 
	float *energyResponsePtr = (float*)energyResponse, *matAttenuationsPtr = (float*)matAttenuations, totalSpectrum=0.0f;

	size_t pitch = projData.pitch;
	size_t slicePitch = pitch * iProjAngles; 
	size_t projPitch = iProjV*slicePitch;
  
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;

	// load spectral data in shared memory
	extern __shared__ float spectralData [];
	for (int idx=threadIdx.x + blockDim.x*threadIdx.y; idx<(nMaterials+1)*nEnergyBins; ){

		if (idx < nEnergyBins)
			spectralData[idx] = energyResponsePtr[idx];
		else
			spectralData[idx] = matAttenuationsPtr[idx-nEnergyBins];

		idx+=blockDim.x*blockDim.y;
	}
	__syncthreads();
	
	//* Re-definition of i-index. This is necessary since the i-index encoded also the z-startV
	int startV = (i/iProjU) * g_detBlockV;
	i = i - ((int) (i/iProjU))*iProjU;
	int endV = (startV + g_detBlockV<iProjV) ? (startV + g_detBlockV) : iProjV;

	for (int e=0; e<nEnergyBins;++e)
		totalSpectrum+=spectralData[e];

	float *crossPath, *refPixel, totalAttenuation, value;
	char *currentProjPtr, *refProjPtr;  

	if ((i<iProjU) && (j<iProjAngles)) {
	  	for (int k=startV; k<endV; ++k){
			  
			currentProjPtr = (char*) projData.ptr +j*pitch +k*slicePitch;
			refProjPtr     = (char*) projData.ptr +j*pitch +k*slicePitch;
			refPixel = (float*) refProjPtr;

			value=0.0f;
			for (int e=0; e<nEnergyBins; ++e){
				totalAttenuation = 0.0f;
				for (int m=0; m<nMaterials; ++m) {
					crossPath = (float*) (currentProjPtr + m*projPitch);
					totalAttenuation += crossPath[i]*spectralData[e+nEnergyBins*(m+1)];
				}
				value += spectralData[e]*exp(-totalAttenuation);
			}

			refPixel[i] = -sign*log(value/totalSpectrum);

		}
	}
}


bool ConePolyFP_optimized(cudaPitchedPtr D_volumeData,
	cudaPitchedPtr D_tmpData,
	cudaPitchedPtr& D_projData,
	const SDimensions3D& dims2, const SConeProjection* angles,
	void* D_energyResponse, void* D_matAttenuations, const SMemoryDimensions3D& spectralInfo,
	float fOutputScale,
	const astra::CMPIProjector3D *mpiPrj = NULL)
{
	//ASTRA_WARN("ConePolyFP_optimized() ::: and then, in dreaming,");
	
	size_t 	       zoffset = 0;
	SDimensions3D  dims    = dims2;
	size_t	  	volByteToJump = 0, projByteToJump = 0; 
	int nMaterials = spectralInfo.nX, nEnergyBins = spectralInfo.nY;  // Materials goes on rows, energy-info on columns

#if USE_MPI
	if(mpiPrj)
	{
		//ASTRA_WARN("ConeFP() ::: The clouds methought would open");
		//Modify the volume properties to ignore the ghostcells
		//Modify the height, and change the startpoint of the copy (zoffset)
		int2 ghosts = mpiPrj->getGhostCells();
		dims.iVolZ -= (ghosts.x + ghosts.y);
		zoffset     = ghosts.x;
		volByteToJump      = ghosts.x * D_volumeData.pitch * D_volumeData.ysize;
		//ASTRA_WARN("ConeFP() ::: and show riches,");
		//Now for the projection data ghostcells
		ghosts 	     = mpiPrj->getGhostCellsPrj();
		dims.iProjV -= (ghosts.x + ghosts.y);  
		projByteToJump      = ghosts.x * D_projData.pitch * D_projData.ysize;
	}
#endif

	SDimensions3D  dimsAllMaterials    = dims2;
	dimsAllMaterials.iVolZ*=nMaterials;
	dimsAllMaterials.iProjV*=nMaterials;

	// Segmentation task
	//cudaEvent_t start, stop;
	//float time;
	//cudaEventCreate(&start);
	//cudaEventCreate(&stop);

	//cudaEventRecord(start, 0);
	//FIXME: tmpDATA should be init here?
	zeroVolumeData(D_tmpData, dimsAllMaterials);
	SegmentationPolicy(D_volumeData, D_tmpData, dims2, D_matAttenuations, D_energyResponse, spectralInfo);
	//cudaEventRecord(stop, 0); cudaEventSynchronize(stop); cudaEventElapsedTime(&time, start, stop);
	//if (mpiPrj->getProcId()==0) ASTRA_WARN("Time for the segmentation: %f ms\n", time);

#if 0 //debugcode
	SDimensions3D  dims3    = dims2;
	//dims3.iVolZ *= nMaterials;
	float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
	copyVolumeFromDevice(pTmpData, D_tmpData, dims3, 0);
	cudaTextForceKernelsCompletion();
	char buff[100];
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	free(pTmpData);
#endif

	if (nMaterials!=1){
		cudaFree(D_projData.ptr);
		cudaPitchedPtr D_projData_allMats = allocateProjectionData(dimsAllMaterials);
		zeroProjectionData(D_projData_allMats, dimsAllMaterials);
		D_projData.ptr = D_projData_allMats.ptr;
	} else {
		zeroProjectionData(D_projData, dims2);
	}

	size_t projPackOffset =  D_projData.pitch * D_projData.ysize * dims2.iProjV;

	// transfer volume to array and bind it to the textures
	cudaArray* cuArray_matVolume = allocateVolumeArray(dims);

	/// TEX: Appena aggiunte
	cudaTextureObject_t coneVolumeTexture;//
	///

#if 0 //debugcode
	SDimensions3D  dims4    = dims;
	float *pTmpData2= (float*)malloc(dims4.iProjU*dims4.iProjAngles*dims4.iProjV*sizeof(float));
	cudaPitchedPtr D_projData3 = D_projData;
	D_projData3.ptr = (char*)D_projData.ptr + projByteToJump;
	//ASTRA_WARN("ConeFP() ::: incr: %d",number);
	copyProjectionsFromDevice(pTmpData2, D_projData3, dims4);
	cudaTextForceKernelsCompletion();
	char buff2[100];
	for (int k=0; k<dims4.iProjV; ++k) {
		sprintf (buff2, "/home/snip/tmp/%d/proj/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff2, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData2[k*dims4.iProjU*dims4.iProjAngles], dims4.iProjU*dims4.iProjAngles*sizeof(float));
		myFile.close();
	}
	free(pTmpData2);
#endif

	//cudaMemset(cuArray_matVolume, 0, dims.iVolX*dims.iVolY*dims.iVolZ*sizeof(float));//

	for (int m=0; m<nMaterials; ++m){
		
		/// TEX: Appena aggiunte
		cudaDeviceSynchronize();
		transferVolumeToArray(D_tmpData, cuArray_matVolume, dims, zoffset);//
		cudaDeviceSynchronize();
		coneVolumeTexture = {0};//

#if 0 //debugcode
	SDimensions3D  dims3    = dims;
	float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
	//transferArrayToHost(pTmpData, cuArray_matVolume, dims3, 0);//
	cudaPitchedPtr D_debugData = allocateVolumeData(dims3);
	zeroVolumeData(D_debugData, dims3);
	transferArrayToVolume(D_debugData, cuArray_matVolume, dims3, 0);//
	copyVolumeFromDevice(pTmpData, D_debugData,  dims3);
	cudaDeviceSynchronize();
	char buff[100];
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	free(pTmpData);
#endif

		cudaDeviceSynchronize();
		bindArrayDataTexture(coneVolumeTexture, cuArray_matVolume);//
		zoffset+=dims2.iVolZ;//
		//cudaTextForceKernelsCompletion();//
		///
		cudaDeviceSynchronize();

		/* TEX: Appena cancellate
		transferVolumeToArray(D_tmpData, cuArray_matVolume, dims, zoffset);
		cudaTextForceKernelsCompletion();
		zoffset+=dims2.iVolZ;

		bindVolumeDataTexture(cuArray_matVolume);
		*/

		//cudaEventRecord(start, 0);
		for (unsigned int iAngle = 0; iAngle < dims.iProjAngles; iAngle += g_MaxAngles) {
			unsigned int iEndAngle = iAngle + g_MaxAngles;
			if (iEndAngle >= dims.iProjAngles)
				iEndAngle = dims.iProjAngles;
	
			cudaPitchedPtr D_subprojData = D_projData;
			D_subprojData.ptr = (char*)D_projData.ptr + iAngle * D_projData.pitch + projByteToJump +m*projPackOffset;
	
			bool ret = PolyConeFP_Array_internal(D_subprojData,
											dims, iEndAngle - iAngle, angles + iAngle, coneVolumeTexture,
											abs(fOutputScale));
			

			if (!ret)
				break;
		}
		//cudaEventRecord(stop, 0); cudaEventSynchronize(stop); cudaEventElapsedTime(&time, start, stop);
		//if (mpiPrj->getProcId()==0) ASTRA_WARN("Time for the FP: %f ms\n", time);
	
		//ASTRA_WARN("ConePolyFP_optimized() ::: that when I waked");
		cudaDeviceSynchronize();
#if USE_MPI
		cudaPitchedPtr D_currProjData = D_projData;
		D_currProjData.ptr = (void*) ((char*) D_projData.ptr +m*projPackOffset);
		//cudaEventRecord(start, 0);
		if(mpiPrj)
		{
			//ASTRA_WARN("ConeFP() ::: I cried to dream again");
			//Note not changed ptr as the exchange function figures this out itself
			const_cast<astra::CMPIProjector3D*>(mpiPrj)->exchangeOverlapAndGhostRegions(NULL, D_currProjData, false, 3);
			const_cast<astra::CMPIProjector3D*>(mpiPrj)->exchangeOverlapAndGhostRegions(NULL, D_currProjData, false, 1);
		}
		//cudaEventRecord(stop, 0); cudaEventSynchronize(stop); cudaEventElapsedTime(&time, start, stop);
		//if (mpiPrj->getProcId()==0) ASTRA_WARN("Time for the FP - exchange: %f ms\n", time);
#endif

	}
#if 0 //debugcode
	SDimensions3D  dims4    = dims;
	float *pTmpData2= (float*)malloc(dims4.iProjU*dims4.iProjAngles*dims4.iProjV*sizeof(float));
	cudaPitchedPtr D_projData3 = D_projData;
	D_projData3.ptr = (char*)D_projData.ptr + projByteToJump;
	//ASTRA_WARN("ConeFP() ::: incr: %d",number);
	copyProjectionsFromDevice(pTmpData2, D_projData3, dims4);
	cudaTextForceKernelsCompletion();
	char buff2[100];
	for (int k=0; k<dims4.iProjV; ++k) {
		sprintf (buff2, "/home/snip/tmp/%d/proj/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff2, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData2[k*dims4.iProjU*dims4.iProjAngles], dims4.iProjU*dims4.iProjAngles*sizeof(float));
		myFile.close();
	}
	free(pTmpData2);
#endif
	//if (1==mpiPrj->getProcId()) 	zeroProjectionData(D_projData, dims2); ///

	//call kernel for sum and log
	float sign = (fOutputScale > 0.0f) ? 1.0f: -1.0f;
	dim3 threadsPerBlock(g_detBlockU, g_anglesPerBlock);
	dim3 numBlocks( ((dims2.iProjU+threadsPerBlock.x-1)/threadsPerBlock.x) * ((dims2.iProjV+g_detBlockV-1)/g_detBlockV), 
		(dims2.iProjAngles+threadsPerBlock.y-1)/threadsPerBlock.y );
	
	//cudaEventRecord(start, 0);
	calculatePolychromaticAttenuation<<<numBlocks, threadsPerBlock, nEnergyBins*(nMaterials+1)*sizeof(float)>>>(D_projData,dims2.iProjU,dims2.iProjAngles,dims2.iProjV, D_energyResponse, D_matAttenuations, nEnergyBins, nMaterials, sign);
	cudaDeviceSynchronize();
	//ASTRA_WARN("ConePolyFP_optimized() ::: ready to drop upon me");

	cudaFreeArray(cuArray_matVolume);

#if 0 //debugcode
	SDimensions3D  dims4    = dims2;
	float *pTmpData2= (float*)malloc(dims4.iProjU*dims4.iProjAngles*dims4.iProjV*sizeof(float));
	cudaPitchedPtr D_projData3 = D_projData;
	D_projData3.ptr = (char*)D_projData.ptr;
	//ASTRA_WARN("ConeFP() ::: incr: %d",number);
	copyProjectionsFromDevice(pTmpData2, D_projData3, dims4);
	cudaTextForceKernelsCompletion();
	char buff2[100];
	for (int k=0; k<dims4.iProjV; ++k) {
		sprintf (buff2, "/home/snip/tmp/%d/proj/%d.raw", mpiPrj->getProcId(), k);
		ofstream myFile (buff2, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData2[k*dims4.iProjU*dims4.iProjAngles], dims4.iProjU*dims4.iProjAngles*sizeof(float));
		myFile.close();
	}
	free(pTmpData2);
#endif

    if (nMaterials!=1) cudaFree((void*)((char*)D_projData.ptr+projPackOffset));

	return true;
}

bool ConePolyFP(cudaPitchedPtr D_volumeData,
	cudaPitchedPtr D_projData,
	const SDimensions3D& dims, const SConeProjection* angles,
	void* D_energyResponse, void* D_matAttenuations, const SMemoryDimensions3D& spectralInfo,
	float fOutputScale,
	const astra::CMPIProjector3D *mpiPrj = NULL)
{
	cudaPitchedPtr D_projData_tmp = allocateProjectionData(dims); 
	SDimensions3D dimsAllMaterials = dims;
	dimsAllMaterials.iVolZ*=spectralInfo.nX;
	cudaPitchedPtr D_volumeData_tmp = allocateVolumeData(dimsAllMaterials); 

	bool ret = ConePolyFP_optimized(D_volumeData, D_volumeData_tmp, D_projData_tmp,
		dims, angles,
		D_energyResponse, D_matAttenuations, spectralInfo,
		fOutputScale,
		mpiPrj);

	duplicateProjectionData(D_projData, D_projData_tmp, dims);
	cudaFree(D_projData_tmp.ptr);
	cudaFree(D_volumeData_tmp.ptr);

	return ret;
}




}


#ifdef SNIP
int main()
{
	astraCUDA3d::SDimensions3D dims;
	dims.iVolX = 256;
	dims.iVolY = 256;
	dims.iVolZ = 256;
	dims.iProjAngles = 32;
	dims.iProjU = 512;
	dims.iProjV = 512;
	dims.iRaysPerDetDim = 1;

	cudaExtent extentV;
	extentV.width = dims.iVolX*sizeof(float);
	extentV.height = dims.iVolY;
	extentV.depth = dims.iVolZ;

	cudaPitchedPtr volData; // pitch, ptr, xsize, ysize

	cudaMalloc3D(&volData, extentV);

	cudaExtent extentP;
	extentP.width = dims.iProjU*sizeof(float);
	extentP.height = dims.iProjV;
	extentP.depth = dims.iProjAngles;

	cudaPitchedPtr projData; // pitch, ptr, xsize, ysize

	cudaMalloc3D(&projData, extentP);
	cudaMemset3D(projData, 0, extentP);

	float* slice = new float[256*256];
	cudaPitchedPtr ptr;
	ptr.ptr = slice;
	ptr.pitch = 256*sizeof(float);
	ptr.xsize = 256*sizeof(float);
	ptr.ysize = 256;

	for (unsigned int i = 0; i < 256*256; ++i)
		slice[i] = 1.0f;
	for (unsigned int i = 0; i < 256; ++i) {
		cudaExtent extentS;
		extentS.width = dims.iVolX*sizeof(float);
		extentS.height = dims.iVolY;
		extentS.depth = 1;
		cudaPos sp = { 0, 0, 0 };
		cudaPos dp = { 0, 0, i };
		cudaMemcpy3DParms p;
		p.srcArray = 0;
		p.srcPos = sp;
		p.srcPtr = ptr;
		p.dstArray = 0;
		p.dstPos = dp;
		p.dstPtr = volData;
		p.extent = extentS;
		p.kind = cudaMemcpyHostToDevice;
		cudaError err = cudaMemcpy3D(&p);
		assert(!err);
	}


	astraCUDA3d::SConeProjection angle[32];
	angle[0].fSrcX = -1536;
	angle[0].fSrcY = 0;
	angle[0].fSrcZ = 200;

	angle[0].fDetSX = 512;
	angle[0].fDetSY = -256;
	angle[0].fDetSZ = -256;

	angle[0].fDetUX = 0;
	angle[0].fDetUY = 1;
	angle[0].fDetUZ = 0;

	angle[0].fDetVX = 0;
	angle[0].fDetVY = 0;
	angle[0].fDetVZ = 1;

#define ROTATE0(name,i,alpha) do { angle[i].f##name##X = angle[0].f##name##X * cos(alpha) - angle[0].f##name##Y * sin(alpha); angle[i].f##name##Y = angle[0].f##name##X * sin(alpha) + angle[0].f##name##Y * cos(alpha); } while(0)
	for (int i = 1; i < 32; ++i) {
		angle[i] = angle[0];
		ROTATE0(Src, i, i*1*M_PI/180);
		ROTATE0(DetS, i, i*1*M_PI/180);
		ROTATE0(DetU, i, i*1*M_PI/180);
		ROTATE0(DetV, i, i*1*M_PI/180);
	}
#undef ROTATE0

	astraCUDA3d::ConeFP(volData, projData, dims, angle, 1.0f);

	float* buf = new float[512*512];

	cudaMemcpy(buf, ((float*)projData.ptr)+512*512*8, 512*512*sizeof(float), cudaMemcpyDeviceToHost);

	printf("%d %d %d\n", projData.pitch, projData.xsize, projData.ysize);

	//saveImage("proj.png", 512, 512, buf);
}
#endif
