/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
           2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Float32SpectralData2D.h"

namespace astra
{

//----------------------------------------------------------------------------------------
// Default constructor
CFloat32SpectralData2D::CFloat32SpectralData2D() :
	CFloat32Data2D() 
{
	m_bInitialized = false;
}

//----------------------------------------------------------------------------------------
// Create an instance of the CFloat32VolumeData2D class, allocating (but not initializing) the data block.
CFloat32SpectralData2D::CFloat32SpectralData2D(int width, int height) 
{
	m_bInitialized = false;
	m_bInitialized = initialize(width, height);
}

//----------------------------------------------------------------------------------------
// Create an instance of the CFloat32VolumeData2D class with initialization of the data.
CFloat32SpectralData2D::CFloat32SpectralData2D(int width, int height, float32* _pfData) 
{
	m_bInitialized = false;
	m_bInitialized = initialize(width, height, _pfData);
}

//----------------------------------------------------------------------------------------
// Create an instance of the CFloat32VolumeData2D class with initialization of the data.
CFloat32SpectralData2D::CFloat32SpectralData2D(int width, int height, float32 _fScalar) 
{
	m_bInitialized = false;
	m_bInitialized = initialize(width, height, _fScalar);
}

//----------------------------------------------------------------------------------------
// Copy constructor
CFloat32SpectralData2D::CFloat32SpectralData2D(const CFloat32SpectralData2D& _other) : CFloat32Data2D(_other)
{
	m_bInitialized = true;
}

//----------------------------------------------------------------------------------------
// Create an instance of the CFloat32VolumeData2D class with pre-allocated data
CFloat32SpectralData2D::CFloat32SpectralData2D(int width, int height, CFloat32CustomMemory* _pCustomMemory)
{
	m_bInitialized = false;
	m_bInitialized = initialize(width, height, _pCustomMemory);
}


// Assignment operator

CFloat32SpectralData2D& CFloat32SpectralData2D::operator=(const CFloat32SpectralData2D& _other)
{
	ASTRA_ASSERT(_other.m_bInitialized);

	*((CFloat32Data2D*)this) = _other;
	m_bInitialized = true;

	return *this;
}

//----------------------------------------------------------------------------------------
// Destructor
CFloat32SpectralData2D::~CFloat32SpectralData2D() 
{
}

//----------------------------------------------------------------------------------------
// Initialization
bool CFloat32SpectralData2D::initialize(int width, int height)
{
	m_bInitialized = _initialize(width, height);
	return m_bInitialized;
}

//----------------------------------------------------------------------------------------
// Initialization
bool CFloat32SpectralData2D::initialize(int width, int height, const float32* _pfData)
{
	m_bInitialized = _initialize(width, height, _pfData);
	return m_bInitialized;
}

//----------------------------------------------------------------------------------------
// Initialization
bool CFloat32SpectralData2D::initialize(int width, int height, float32 _fScalar)
{
	m_bInitialized = _initialize(width, height, _fScalar);
	return m_bInitialized;
}

//----------------------------------------------------------------------------------------
// Initialization
bool CFloat32SpectralData2D::initialize(int width, int height, CFloat32CustomMemory* _pCustomMemory) 
{
	m_bInitialized = _initialize(width, height,  _pCustomMemory);
	return m_bInitialized;
}


} // end namespace astra
