/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
           2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include <cstdio>
#include <cassert>

#include "polySirt3d.h"
#include "util3d.h"
#include "arith3d.h"
#include "cone_fp.h"
#include "helpers.h"

#ifdef STANDALONE
#include "testutil.h"
#endif


namespace astraCUDA3d {

polySIRT::polySIRT() : ReconAlgo3D() {
	D_maskData.ptr = 0;
	D_smaskData.ptr = 0;

	D_sinoData.ptr = 0;
	D_volumeData.ptr = 0;

	D_projData.ptr = 0;
	D_tmpData.ptr = 0;

	D_lineWeight.ptr = 0;
	D_pixelWeight.ptr = 0;

	useVolumeMask = false;
	useSinogramMask = false;

	useMinConstraint = true;
	useMaxConstraint = false;

	isSpectralInfoReceived = false;
	applyBBstep = false;
	isFirstIteration = true;
	}


polySIRT::~polySIRT() {
	reset();
	}

void polySIRT::reset() {
	cudaFree(D_projData.ptr);
	cudaFree(D_tmpData.ptr);
	cudaFree(D_lineWeight.ptr);
	cudaFree(D_pixelWeight.ptr);
	cudaFree(D_energyResponse);
	cudaFree(D_matAttenuations);

	D_maskData.ptr = 0;
	D_smaskData.ptr = 0;

	D_sinoData.ptr = 0;
	D_volumeData.ptr = 0;

	D_projData.ptr = 0;
	D_tmpData.ptr = 0;

	D_lineWeight.ptr = 0;
	D_pixelWeight.ptr = 0;

	D_energyResponse = 0;
	D_matAttenuations = 0;

	useVolumeMask = false;
	useSinogramMask = false;
	isSpectralInfoReceived = false;
	isFirstIteration = true;

	if (applyBBstep){
		cudaFree(D_prevVolumeData.ptr); 
		cudaFree(D_prevTmpData.ptr);
		D_prevVolumeData.ptr=0; 
		D_prevTmpData.ptr=0;
		applyBBstep = false;
	}

	ReconAlgo3D::reset();
	}

bool polySIRT::enableVolumeMask() {
	useVolumeMask = true;
	return true;
	}

bool polySIRT::enableSinogramMask() {
	useSinogramMask = true;
	return true;
	}


bool polySIRT::init() {
	//ASTRA_WARN("polySIRT::init() ::: Be not afeard; the isle is full of noises, ");

	D_pixelWeight = allocateVolumeData(dims);
	zeroVolumeData(D_pixelWeight, dims);

	//ASTRA_WARN("SIRT::init() ::: Sounds, and sweet airs, ");

	assert(isSpectralInfoReceived); // D_tmpData will be recycled for multiple purposes, so will have a different size
	multipleMaterialsDims = dims;
	multipleMaterialsDims.iVolZ *= spectralInfo.nX;
	D_tmpData = allocateVolumeData(multipleMaterialsDims);
	zeroVolumeData(D_tmpData, multipleMaterialsDims);

	//ASTRA_WARN("SIRT::init() ::: that give delight and hurt not. ");

	D_projData = allocateProjectionData(dims);
	zeroProjectionData(D_projData, dims);

	//ASTRA_WARN("SIRT::init() ::: Sometimes a thousand twangling instruments ");

	D_lineWeight = allocateProjectionData(dims);
	zeroProjectionData(D_lineWeight, dims);

	//ASTRA_WARN("SIRT::init() ::: Will hum about mine ears; ");

	// We can't precompute lineWeights and pixelWeights when using a mask
	if (!useVolumeMask && !useSinogramMask)
		precomputeWeights();

	//ASTRA_WARN("SIRT::init() ::: and sometime voices ");

	// TODO: check if allocations succeeded
	return true;
	}

bool polySIRT::setMinConstraint(float fMin) {
	fMinConstraint = fMin;
	useMinConstraint = true;
	return true;
	}

bool polySIRT::setMaxConstraint(float fMax) {
	fMaxConstraint = fMax;
	useMaxConstraint = true;
	return true;
	}

bool polySIRT::precomputeWeights() {
	//ASTRA_WARN("SIRT::precomputeWeights() ::: FUll fathon five thy father lies; ");

	zeroProjectionData(D_lineWeight, dims);
	//ASTRA_WARN("SIRT::precomputeWeights() ::: 1");
	if (useVolumeMask) {
		//ASTRA_WARN("SIRT::precomputeWeights() ::: 2");
		callFP(D_maskData, D_lineWeight, 1.0f);
	} else {
		//ASTRA_WARN("SIRT::precomputeWeights() ::: 3");
		processVol3D<opSet>(D_tmpData, 1.0f, dims);
		//ASTRA_WARN("SIRT::precomputeWeights() ::: 4");
		callFP(D_tmpData, D_lineWeight, 1.0f);
	}

	//ASTRA_WARN("SIRT::precomputeWeights() ::: Of his bones are coral made; ");
	
	processSino3D<opInvert>(D_lineWeight, dims);

	if (useSinogramMask) {
		// scale line weights with sinogram mask to zero out masked sinogram pixels
		processSino3D<opMul>(D_lineWeight, D_smaskData, dims);
	}

	//ASTRA_WARN("SIRT::precomputeWeights() ::: Those are pearls that were his eyes ");

	zeroVolumeData(D_pixelWeight, dims);

	if (useSinogramMask) {
		callBP(D_pixelWeight, D_smaskData, 1.0f);
	} else {
		processSino3D<opSet>(D_projData, 1.0f, dims);
		callBP(D_pixelWeight, D_projData, 1.0f);
	}

	//ASTRA_WARN("SIRT::precomputeWeights() ::: Nothing of him that doth fade,");
	#if 0
	float* bufp = new float[512*512];

	for (int i = 0; i < 180; ++i) {
		for (int j = 0; j < 512; ++j) {
			cudaMemcpy(bufp+512*j, ((float*)D_projData.ptr)+180*512*j+512*i, 512*sizeof(float), cudaMemcpyDeviceToHost);
		}

		char fname[20];
		sprintf(fname, "ray%03d.png", i);
		saveImage(fname, 512, 512, bufp);
	}
	#endif

	#if 0
	float* buf = new float[256*256];

	for (int i = 0; i < 256; ++i) {
		cudaMemcpy(buf, ((float*)D_pixelWeight.ptr)+256*256*i, 256*256*sizeof(float), cudaMemcpyDeviceToHost);

		char fname[20];
		sprintf(fname, "pix%03d.png", i);
		saveImage(fname, 256, 256, buf);
	}
	#endif
	processVol3D<opInvert>(D_pixelWeight, dims);

	if (useVolumeMask) {
		// scale pixel weights with mask to zero out masked pixels
		processVol3D<opMul>(D_pixelWeight, D_maskData, dims);
	}
	//ASTRA_WARN("SIRT::precomputeWeights() ::: But doth suffer a sea change");
	//ASTRA_WARN("SIRT::precomputeWeights() ::: Into something rich and strange.");

	return true;
	}


bool polySIRT::setVolumeMask(cudaPitchedPtr& _D_maskData) {
	assert(useVolumeMask);

	D_maskData = _D_maskData;

	return true;
	}

bool polySIRT::setSinogramMask(cudaPitchedPtr& _D_smaskData){
	assert(useSinogramMask);

	D_smaskData = _D_smaskData;

	return true;
	}

bool polySIRT::setBuffers(cudaPitchedPtr& _D_volumeData,
                      cudaPitchedPtr& _D_projData) {
	D_volumeData = _D_volumeData;
	D_sinoData = _D_projData;

	return true;
	}

void polySIRT::BBstep(cudaPitchedPtr x, cudaPitchedPtr prev_x, cudaPitchedPtr g, cudaPitchedPtr prev_g,  const SDimensions3D& dims) {

	//float norm_gradients=0.0f, BBStepValue=0.0f; // || g_k-g_(k-1) ||^2_2
	float BBStepValue=0.0f, norm_gradients=0.0f, *d_norm_gradients, *d_BBStepValue;
	cudaMalloc(&d_norm_gradients, sizeof(float));   cudaMalloc(&d_BBStepValue, sizeof(float)); 
	cudaMemset(d_norm_gradients, 0, sizeof(float)); cudaMemset(d_BBStepValue, 0, sizeof(float));

	#if 0 //debugcode
		//if (mpiPrj->getProcId()==2){
		SDimensions3D  dims3    = dims;
		float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
		copyVolumeFromDevice(pTmpData, x, dims3, 0);
		cudaTextForceKernelsCompletion();
		char buff[100];
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/x/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		copyVolumeFromDevice(pTmpData, prev_x, dims3, 0);
		cudaTextForceKernelsCompletion();
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/prev_x/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		copyVolumeFromDevice(pTmpData, g, dims3, 0);
		cudaTextForceKernelsCompletion();
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/g/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		copyVolumeFromDevice(pTmpData, prev_g, dims3, 0);
		cudaTextForceKernelsCompletion();
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/prev_g/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		free(pTmpData);
		//}
	#endif
	dim3 blockSize(32,8);
	dim3 gridSize((dims.iVolX+blockSize.x-1)/blockSize.x, (dims.iVolY+blockSize.y-1)/blockSize.y);

	k_subtractVolumes<<<gridSize, blockSize>>>(g, prev_g, d_norm_gradients, dims.iVolX, dims.iVolY, dims.iVolZ);
	cudaTextForceKernelsCompletion();
	//ASTRA_WARN("polySIRT::BBstep() ::: 1");

	cudaMemcpy(&norm_gradients, d_norm_gradients, sizeof(float), cudaMemcpyDeviceToHost);
	//ASTRA_WARN("polySIRT::BBstep() ::: norm_gradients: %f",norm_gradients);
	float global_norm_gradients=0.0f;
	MPI_Allreduce(&norm_gradients,&global_norm_gradients, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	//ASTRA_WARN("polySIRT::BBstep() ::: global_norm_gradients: %f",global_norm_gradients);
	cudaMemcpy(d_norm_gradients, &global_norm_gradients, sizeof(float), cudaMemcpyHostToDevice);

	if (isFirstIteration) {
		BBStepValue = 0.01f;
		cudaMemcpy(d_BBStepValue, &BBStepValue, sizeof(float), cudaMemcpyHostToDevice);
	} else {
		k_BBstep<<<gridSize, blockSize>>>(x, prev_x, prev_g, d_norm_gradients, d_BBStepValue, dims.iVolX, dims.iVolY, dims.iVolZ);
		cudaTextForceKernelsCompletion();	
	}
	//ASTRA_WARN("polySIRT::BBstep() ::: 2");

	cudaMemcpy(&BBStepValue, d_BBStepValue, sizeof(float), cudaMemcpyDeviceToHost);
	//ASTRA_WARN("polySIRT::BBstep() ::: BB step value: %f",BBStepValue);
	float global_BBStepValue=0.0f;
	MPI_Allreduce(&BBStepValue,&global_BBStepValue, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	//if (mpiPrj->getProcId()==0)
	//	ASTRA_WARN("polySIRT::BBstep() ::: BB-step value: %f",global_BBStepValue);
	cudaMemcpy(d_BBStepValue, &global_BBStepValue, sizeof(float), cudaMemcpyHostToDevice);

	k_lastBBstep<<<gridSize, blockSize>>>(x, prev_x, g, prev_g, d_BBStepValue, dims.iVolX, dims.iVolY, dims.iVolZ);
	cudaTextForceKernelsCompletion();


	#if 0 //debugcode
		//if (mpiPrj->getProcId()==2){
		SDimensions3D  dims3    = dims;
		float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
		copyVolumeFromDevice(pTmpData, x, dims3, 0);
		cudaTextForceKernelsCompletion();
		char buff[100];
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/x/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		copyVolumeFromDevice(pTmpData, prev_x, dims3, 0);
		cudaTextForceKernelsCompletion();
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/prev_x/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		copyVolumeFromDevice(pTmpData, g, dims3, 0);
		cudaTextForceKernelsCompletion();
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/g/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		copyVolumeFromDevice(pTmpData, prev_g, dims3, 0);
		cudaTextForceKernelsCompletion();
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/prev_g/slice_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		free(pTmpData);
		//}
	#endif
		cudaFree(d_norm_gradients);  cudaFree(d_BBStepValue);
	}


bool polySIRT::iterate(unsigned int iterations)
{
	shouldAbort = false;
	if (useVolumeMask || useSinogramMask)
		precomputeWeights();

	// iteration
	for (unsigned int iter = 0; iter < iterations && !shouldAbort; ++iter) {
		// copy sinogram to projection data
		if (mpiPrj->getProcId()==0) ASTRA_WARN("polySIRT::iterate() ::: iteration %d of %d",iter+1,iterations);

#if 1// MPI
        //zeroProjectionData(D_projData, dims); // it occurs inside polyFP now
#else
        duplicateProjectionData(D_projData, D_sinoData, dims);
#endif

		// compute - W * x
		//
		callPolyFP(D_volumeData, D_tmpData, D_projData, -1.0f, D_energyResponse, D_matAttenuations, spectralInfo);

#if 0 //debugcode
	//if (mpiPrj->getProcId()==0){
		SDimensions3D  dims3    = dims;
		float *pTmpData= (float*)malloc(dims3.iProjU*dims3.iProjAngles*dims3.iProjV*sizeof(float));
		copyProjectionsFromDevice(pTmpData, D_projData, dims3);
		cudaTextForceKernelsCompletion();
		char buff[100];
		for (int k=0; k<dims3.iProjV; ++k) {
			sprintf (buff, "/home/snip/tmp/%d/proj/%d.raw", mpiPrj->getProcId(),k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iProjU*dims3.iProjAngles], dims3.iProjU*dims3.iProjAngles*sizeof(float));
			myFile.close();
		}
		free(pTmpData);
	//}
#endif

#if 1 // MPI
		// compute (p - W*x)
		//
        processSino3D<opAddScaled>(D_projData, D_sinoData, 1.0f, dims);
#endif

#if 0 //debugcode
	if (mpiPrj->getProcId()==1){
		SDimensions3D  dims3    = dims;
		float *pTmpData= (float*)malloc(dims3.iProjU*dims3.iProjAngles*dims3.iProjV*sizeof(float));
		copyProjectionsFromDevice(pTmpData, D_projData, dims3);
		cudaTextForceKernelsCompletion();
		char buff[100];
		for (int k=0; k<dims3.iProjV; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/proj_difference/proj_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iProjU*dims3.iProjAngles], dims3.iProjU*dims3.iProjAngles*sizeof(float));
			myFile.close();
		}
		free(pTmpData);
	}
#endif

		// compute R*(p - W*x)
		//
		processSino3D<opMul>(D_projData, D_lineWeight, dims);

#if 0 //debugcode
	if (mpiPrj->getProcId()==1){
		SDimensions3D  dims3    = dims;
		float *pTmpData= (float*)malloc(dims3.iProjU*dims3.iProjAngles*dims3.iProjV*sizeof(float));
		copyProjectionsFromDevice(pTmpData, D_projData, dims3);
		cudaTextForceKernelsCompletion();
		char buff[100];
		for (int k=0; k<dims3.iProjV; ++k) {
			sprintf (buff, "/data/home/diuso/tmp/proj_weighted/proj_%d.raw", k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iProjU*dims3.iProjAngles], dims3.iProjU*dims3.iProjAngles*sizeof(float));
			myFile.close();
		}
		free(pTmpData);
	}
#endif
		// compute Wt*R*(p - W*x)
		//
		zeroVolumeData(D_tmpData, dims);
		callBP(D_tmpData, D_projData, 1.0f);

		if (!applyBBstep) {

			// compute C*Wt*R*(p - W*x)
			//
			processVol3D<opAddMulScaled>(D_volumeData, D_tmpData, D_pixelWeight,1.0f, dims);

		} else {

			// compute gamma*Wt*R*(p - W*x)
			//
			//processVol3D<opAddScaled>(D_volumeData, D_tmpData, BBstep, dims);
			
			// ### stai attento ai segni ###
			//
			// x, x_prev, g, g_prev
			//if (mpiPrj->getProcId()==1)
			BBstep(D_volumeData, D_prevVolumeData, D_tmpData, D_prevTmpData, dims);
		}
		
		if (useMinConstraint)
			processVol3D<opClampMin>(D_volumeData, fMinConstraint, dims);
		if (useMaxConstraint)
			processVol3D<opClampMax>(D_volumeData, fMaxConstraint, dims);

		isFirstIteration=false;
	}

	return true;
}

float polySIRT::computeDiffNorm()
{
	// copy sinogram to projection data
	duplicateProjectionData(D_projData, D_sinoData, dims);

	// do FP, subtracting projection from sinogram
	if (useVolumeMask) {
			duplicateVolumeData(D_tmpData, D_volumeData, dims);
			processVol3D<opMul>(D_tmpData, D_maskData, dims);
			callFP(D_tmpData, D_projData, -1.0f);
	} else {
			callFP(D_volumeData, D_projData, -1.0f);
	}

	float s = dotProduct3D(D_projData, dims.iProjU, dims.iProjAngles, dims.iProjV);
	return sqrt(s);
}

bool polySIRT::SetSpectralInformation(const float* pfEnergyResponse, const float* pfMaterialAttenuations, const SMemoryDimensions3D& spectralInfo_)
{
	spectralInfo = spectralInfo_;

	cudaMalloc((void**)&D_energyResponse, sizeof(float)*spectralInfo.nY);
	cudaMemcpy(D_energyResponse, (void*) pfEnergyResponse, sizeof(float)*spectralInfo.nY, cudaMemcpyHostToDevice);

	cudaMalloc((void**)&D_matAttenuations,sizeof(float)*spectralInfo.nX*spectralInfo.nY);
	cudaMemcpy(D_matAttenuations, (void*) pfMaterialAttenuations, sizeof(float)*spectralInfo.nX*spectralInfo.nY, cudaMemcpyHostToDevice);

	isSpectralInfoReceived = true;
	return true;
}

bool polySIRT::SetBBStepOptimization(bool value) {
	applyBBstep = value;

	D_prevVolumeData = allocateVolumeData(dims);
	D_prevTmpData    = allocateVolumeData(dims);

	zeroVolumeData(D_prevVolumeData, dims);
	zeroVolumeData(D_prevTmpData, dims);

	return true;
}

// FIXME: to update
bool doPolySIRT(cudaPitchedPtr& D_volumeData, 
            cudaPitchedPtr& D_sinoData,
            cudaPitchedPtr& D_maskData,
            const SDimensions3D& dims, const SConeProjection* angles,
            unsigned int iterations)
{
	polySIRT sirt;
	bool ok = true;

	ok &= sirt.setConeGeometry(dims, angles, 1.0f);
	if (D_maskData.ptr)
		ok &= sirt.enableVolumeMask();

	if (!ok)
		return false;

	ok = sirt.init();
	if (!ok)
		return false;

	if (D_maskData.ptr)
		ok &= sirt.setVolumeMask(D_maskData);

	ok &= sirt.setBuffers(D_volumeData, D_sinoData);
	if (!ok)
		return false;

	ok = sirt.iterate(iterations);

	return ok;
}




}